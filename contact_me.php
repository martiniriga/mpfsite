<?php
$captcha = "";
if(isset($_POST['g-recaptcha-response']))
    $captcha=($_POST['g-recaptcha-response']);
    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeEthwTAAAAACgQ5VIRR0ybfiKqEVshiZUOjCI5&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
if($response["success"] != false)
{
if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
    // invalid emailaddress
$output = json_encode(array( //create JSON data
             'type'=>'error',
             'text' => 'Invalid email address'
         ));
         die($output); //exit script outputting json data
}

    $email     = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
    $to_email  = "info@masterpiecenet.co.ke"; //Recipient email

     //check if its an ajax request, exit if not
     if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {

         $output = json_encode(array( //create JSON data
             'type'=>'error',
             'text' => 'Sorry Request must be Ajax POST'
         ));
         die($output); //exit script outputting json data
     }

     //Sanitize input data using PHP filter_var().
     $fullname  = filter_var($_POST["fullname"], FILTER_SANITIZE_STRING);

     $subject   = filter_var($_POST["subject"], FILTER_SANITIZE_STRING);
     $message   = filter_var($_POST["message"], FILTER_SANITIZE_STRING);

        $eol = PHP_EOL;
     //proceed with PHP email.
     $headers = 'From: '.$fullname.' '.$eol.
     'Reply-To: '.$email.' ' . $eol .
     'X-Mailer: PHP/' . phpversion();
          $send_mail = mail($to_email, $subject, $message, $headers);

     if(!$send_mail)
     {
         //If mail couldn't be sent output error. Check your PHP email configuration (if it ever happens)
         $output = json_encode(array('type'=>'error', 'text' => 'Could not send mail! Please check your PHP mail configuration.'));
         die($output);
     }else{
         $output = json_encode(array('type'=>'success', 'text' => 'Hi '.$fullname .' your email has been sent successfully.'));
         die($output);
     }
}
else{
     $output = json_encode(array('type'=>'error', 'text' => 'Invalid Captcha.'));
         die($output);;
}
?>
