 $(document).ready(function () {
      var trigger = $('.hamburger'),
      overlay = $('.overlay'),
      isClosed = false;

      trigger.click(function () {
          hamburger_cross();      
      });

      function hamburger_cross() {

          if (isClosed == true) {          
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            $('.logo').removeClass('is-open');            
            isClosed = false;
        } else {   
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            $('.logo').addClass('is-open');

            isClosed = true;
        }
    }

    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
    }); 

    $(window).scroll(function () {
      //if you hard code, then use console
      //.log to determine when you want the 
      //nav bar to stick.  
      console.log($(window).scrollTop())
    if ($(window).scrollTop() > 90) {
      $('.navbar').addClass('navbar-inverse');
    }
    if ($(window).scrollTop() < 91) {
      $('.navbar').removeClass('navbar-inverse');
    }
  }); 
});